<p align="center">
  <a href="https://www.babbel.com">
    <img alt="Babbel favicon" src="https://d33wubrfki0l68.cloudfront.net/673084cc885831461ab2cdd1151ad577cda6a49a/92a4d/static/images/favicon.png" width="60" />
  </a>
</p>
<h1 align="center">
    Career Ladder at Babbel
</h1>

This is a static site that displays the career ladder that Babbel use internally 🙌

It's built in Gatsby, and deployed using Netlify.

## 🚀 Quick start

You'll need the Gatsby CLI on your machine, so if you don't have it, install it!
``` sh
npm install --global gatsby-cli
```

Next, navigate to the `eng-career-ladder` directory and use yarn to get everything installed

``` sh
cd eng-career-ladder
yarn
```

If you're developing locally, you can start up the site locally with hot loading ⚡
``` sh
gatsby develop
```

Finally, you can build everything into a set of static files 🛠️
``` sh
gatsby build
```
