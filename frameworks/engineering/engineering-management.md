---
path: "/engineering/management"
title: "Engineering Management"
sidebarTitle: "Engineering Management"
sidebarGroup: "engineering"
yaml: true
levels: 5
levels_values: ["EM", "Senior EM"]
homepage: true
topics:
  - name: "Individual excellence"
    title: "Individual excellence"
    content:
      - level: 'EM'
        criteria:
          - Attract, develop, motivate and retain productive and engaged employees
          - Manage and communicate the timeline, risks and lead delivery of initiatives
          - Provide actionable feedback timely
          - Conduct and evaluate candidates ensuring team fit and soft skills, participate in all stages of talent acquisition
          - Follows a data-informed approach in decision making
          - Familiar with coaching frameworks and applies them to their relationship with direct reports
          - Able to manage and resolve conflicts between direct reports and stakeholders
          - Comfortable in engaging heterogeneous mid-sized audiences with their communication skills
          - Knowledgable about organizational design and processes
      - level: 'Senior EM'
        criteria:
          - Able to set goals balancing immediate and long-term impact on business
          - Proactively manage and resolve conflicts
          - Have a clear understanding on how the business processes work.
          - Enable business plans via resource and hiring planning
          - Experienced in engaging heterogeneous mid and large-sized audiences
  - name: "Impact on the team"
    title: "Impact on the team"
    content:
      - level: 'EM'
        criteria:
          - Establish safe environment for their teams
          - Ensures timely delivery of team's objectives
          - Navigate the team through the stages of forming, storming, norming and performing
          - Understands the principles of delegation and applies it from project management to talent development in their team
          - Able to road-manage on large projects with others working on them
          - Can identify, avoid and help resolve cross-team dependencies and issues
      - level: 'Senior EM'
        criteria:
          - Create and grow the next generation of leaders in the organization
          - Drive adoption of data-informed decision making
          - Guide the objective-setting process for all of their teams
          - Foster cross-team synergies and reduce effect of working in silos
  - name: "Reach beyond the team"
    title: "Reach beyond the team"
    content:
      - level: 'EM'
        criteria:
          - Start cross teams initiatives for knowledge sharing or delivery
          - Shows a broad understanding of the company's business
          - Proactively explains how new techs may open new business opportunities
          - Contributes to short to medium-term direction, able to look ahead 3 - 6 months and identify the areas of greatest impact based on company objectives
      - level: 'Senior EM'
        criteria:
          - Has deep understanding of the company's business, understand market landscape
          - Participate in medium-term strategic planning, able to look ahead 6 - 12 months and identify the areas of greatest impact based on company objectives
          - Actively work together with the responsible department to improve employer branding and workplace experience
---
### About our engineering career ladder
The engineering career ladder is a tool that helps engineers and managers:
- make development and career plans
- talk about what we’re looking for from engineers in a consistent way
- set a fair level of compensation.

The framework is a compass, not a GPS.

It's meant to be helpful. It's not meant to be a rating system for humans, free from edge cases.

### How does it work?
The framework covers all the things we’re looking for from engineers at Babbel. We’re interested in these 3 dimensions:
- Individual excellence
- Impact on the team
- Reach beyond the team

We sort them into five levels:
- EM
- Senior EM

Your manager will work with you on this. None of it will happen mysteriously behind closed doors. You’ll agree what level of progression you’re going for and what you need to improve on with your manager. It should be clear how you’re doing relative to that at all times.

### Things to keep in mind
- There are many different ways to progress and be valuable to Babbel as you grow, including deep technical knowledge and ability, technical leadership and people management. All are equally valuable paths in every engineering team.
- The framework represents a career’s worth of progression, people shouldn’t expect to fly up it in 18 months!
- Engineering progression isn’t an exact science and there will always be some ambiguity.
- This isn’t a checklist – it’s possible to progress up a level without showing all the behaviours in that level.
- There will be levels on top (eg ‘Inventor of Android’ or ‘Author of Go’), but we won’t add them until we need them.
- You can find some more information in these links. If that doesn't answer most of your questions, please ask your manager.

### Give us your feedback!
This is only the first version of our framework and we really want your feedback.

We're particularly keen to add as many examples to the behaviours as possible, to further clarify them.
